﻿namespace _2dCurve
{
    using System;
    using System.Windows;
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private const int ZeroOffsetPointX = 80;
        private const int ZeroOffsetPointY = 320;
        private const int CurveLength = 550;

        private double firstUserNumber;
        private double secondUserNumer;
        private double thirdUserNumber;

        public MainWindow()
        {
            this.InitializeComponent();
        }

        private void BtnInfo_Click(object sender, RoutedEventArgs e)
        {
            TextBoxFirstNumber.Text = "0,001";
            TextBoxSecondNumber.Text = "0,005";
            TextBoxThirdNumber.Text = "5";
            /// MessageBox.Show("Your data has been reset!", "Tip");
        }

        private void ClearCurve()
        {
            MainCurve.Points.Clear();
        }

        private void DrawCurve()
        {
            double firstNumber = -this.firstUserNumber;
            double secondNumber = -this.secondUserNumer;
            double thirdNumber = -this.thirdUserNumber;

            double x = 0;
            double y;

            for (int i = 0; i < CurveLength; i++)
            {
                y = ((x * x) * firstNumber) + (x * secondNumber) + thirdNumber;
                x += 1;

                MainCurve.Points.Add(new Point(x + ZeroOffsetPointX, y + ZeroOffsetPointY));
            }
        }

        private double MyParse(string value)
        {
            double result = 0;
            try
            {
                if (value.EndsWith(","))
                {
                    // Show message box with info.
                    /// MessageBox.Show("Add more numbers.", "Tip");
                }
                else
                {
                    result = Convert.ToDouble(value);
                }
            }
            catch (FormatException)
            {
                if (value.EndsWith("."))
                {
                    MessageBox.Show("Use , instead of .", "Tip");
                }
                else if (string.IsNullOrEmpty(value))
                {
                    // Show message box with info.
                    /// MessageBox.Show("Don't leave empty fields", "Tip");
                }
                else
                {
                    MessageBox.Show("Use only numbers", "Tip");
                }
            }
            catch (OverflowException)
            {
                MessageBox.Show("'{0}' is outside the range of a Double.", value);
            }

            return result;
        }

        private void TextBoxFirstNumber_TextChanged(object sender, TextChangedEventArgs e)
        {
            this.ClearCurve();

            this.firstUserNumber = this.MyParse(TextBoxFirstNumber.Text);

            this.DrawCurve();
        }

        private void TextBoxSecondNumber_TextChanged(object sender, TextChangedEventArgs e)
        {
            this.ClearCurve();

            this.secondUserNumer = this.MyParse(TextBoxSecondNumber.Text);

            this.DrawCurve();
        }

        private void TextBoxThirdNumber_TextChanged(object sender, TextChangedEventArgs e)
        {
            this.ClearCurve();

            this.thirdUserNumber = this.MyParse(TextBoxThirdNumber.Text);

            this.DrawCurve();
        }
    }
}
